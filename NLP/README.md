# oneflow_nlp_model

In this repo, we supply several NLP models and NLP ops based on OneFlow, including:

* TextCNN, Bi-LSTM for text classification
* RoBERT


## License
[Apache License 2.0](LICENSE)
