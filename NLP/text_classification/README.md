# text classification

- imdb：处理成 OFRecord 格式的 [IMDB](https://ai.stanford.edu/~amaas/data/sentiment/) 数据集 
- utils： 读取数据集、处理序列等常用组件
- textcnn：在 imdb 数据集上训练的 TextCNN 模型
- bilstm：在 imdb 数据集上训练的 Bi-LSTM 模型
