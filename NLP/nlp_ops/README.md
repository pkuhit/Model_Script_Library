# nlp_ops

为 OneFlow 开发的 NLP 算子，使用时需把 nlp_ops 所在的目录加入环境变量，例如：

```python
import sys

sys.path.append( <nlp_ops 所在的目录> )
from nlp_ops import bilstm

```
