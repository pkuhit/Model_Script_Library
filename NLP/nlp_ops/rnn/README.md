# 各种 RNN

```bash
recurrent.py    # 基础的循环结构，可以配合各种 Cell 使用
lstm.py         # 实现了 lstm, bilstm 和 LSTMCell
simple_rnn.py   # 实现了 simple_rnn 和 SimpleRNNCell
```

设计时参考了 tensorflow.keras.layers 中的 RNN、LSTM、LSTMCell、SimpleRNN、SimpleRNNCell